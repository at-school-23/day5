package ru.devray.day5.abstractclass;

public abstract class MyPlayableAbstractCLass {

        abstract void play();

        abstract int doStuff();

}
