package ru.devray.day5.abstractclass;

public class DemoInterface {
    public static void main(String[] args) {
        Playable[] playables = {
                new Mp3Player(),
                new Uno()
        };

        for (Playable p : playables) {
            p.play();
        }
    }
}
