package ru.devray.day5.enums;

import java.util.Arrays;

public class Casino {
    public static void main(String[] args) {
        PlayingCard playingCard = new PlayingCard("K", CardType.ЧЕРВЫ);

        //enum - switch-case

        CardType[] values = CardType.values();
        System.out.println(Arrays.toString(values));

        CardType cardType = playingCard.kind;
        System.out.println(cardType.ordinal());

        //start   getInfo
        String input = "червы";
        CardType cardType1 = CardType.valueOf(input.toUpperCase());


        switch (playingCard.kind) {
            case ЧЕРВЫ:
                System.out.println("------3244234");
                break;
            case ПИКИ:
                System.out.println("это карта пиковая");
        }
    }
}
