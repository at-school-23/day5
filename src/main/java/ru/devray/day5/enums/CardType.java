package ru.devray.day5.enums;

public enum CardType {
    ЧЕРВЫ("hearts", 2),
    БУБЫ("diamonds", 3),
    ПИКИ("spades", 6),
    КРЕСТИ("clubs", 7);

    String englishAlias;
    int rating;

    CardType(String englishAlias, int rating) {
        this.englishAlias = englishAlias;
        this.rating = rating;
    }

    @Override
    public String toString() {
        return "CardType{" +
                "englishAlias='" + englishAlias + '\'' +
                ", rating=" + rating +
                '}';
    }
}
