package ru.devray.day5.enums;

public class PlayingCard {

    String nominal;
    CardType kind;
//    String kind;

    public PlayingCard(String nominal, CardType kind) {
        this.nominal = nominal;
        this.kind = kind;
    }
}
