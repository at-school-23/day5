package ru.devray.day5.interfaces;

public class SmartPhone implements Navigable, ITurnOnAble {
    @Override
    public void navigateTo(String address) {

    }

    @Override
    public void turnOn() {
        Navigable.super.turnOn();
    }

    //turnOn
    //turnOff
}
