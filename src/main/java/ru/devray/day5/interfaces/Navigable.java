package ru.devray.day5.interfaces;

public interface Navigable {
    void navigateTo(String address);
    default void turnOn(){
        System.out.println();
    }
}
