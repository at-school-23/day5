package ru.devray.day5.interfaces;

public class InterfDemo {
    public static void main(String[] args) {
        SmartPhone sm1 = new SmartPhone(); //Telephone - CommunicationDevice
        SmartPhone sm2 = new SmartPhone();

        ITurnOnAble i1 = sm1;
        i1.turnOn();
        Navigable n1 = sm1;
        n1.navigateTo("");
    }
}
