package ru.devray.day5.interfaces;

public class NavigationComputer {

    public String brand;
    public String cpuModel;
    public boolean hasOfflineMaps;

    public int calculatePath() {
        return (int) (Math.random() * 10);
    }

    public void turnOn() {
        System.out.println("Навигатор включен! Ищу спутники...");
    }

    public void turnOff() {
        System.out.println("Навигатор выключен! Пока, спутники!");
    }

}
