package ru.devray.day5.interfaces;

public class ElectricDevice {

    String deviceName;

    public void turnOn() {
        System.out.println("Аппарат включен!");
    }

    public void turnOff() {
        System.out.println("Аппарат выключен!");
    }

}
